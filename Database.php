<?php

namespace FpDbTest;

use mysqli;

class Database implements DatabaseInterface
{
    private mysqli $mysqli;

    private const string NULL_VALUE = 'NULL';

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    public function buildQuery(string $query, array $args = []): string
    {
        return $this
            ->buildSubQuery($query, $args)
            ->query;
    }

    public function skip(): SkipValue
    {
        return new SkipValue;
    }

    /**
     * @throws \Exception
     */
    private function buildSubQuery(string $query, array $args): ReplacementResult
    {
        $argIndex = 0;
        $skippedValues = false;

        // Парсим запрос на предмет идентификаторов или блоков с идентификаторами
        // блоки могут быть рекурсивными, т.е. включать в себя другие условные блоки
        $query = preg_replace_callback('/(\?(?:[dfa#]{1}|\s?))|(?<cond>{[^}]*[^{]*})/u', function (array $matches) use ($args, &$argIndex, &$skippedValues) {
            if (count($args) <= $argIndex) {
                // плейсхолдеры в запросе продолжаются, а аргументы закончились - непорядок
                throw new \Exception('Not enough args for replacements');
            }

            if (!isset($matches['cond'])) {
                try {
                    return $this->replaceId($matches[0], $args[$argIndex++]);
                } catch (SkipValueException $e) {
                    $skippedValues = true;
                    return '';
                }
            } else {
                // убираем крайние фигурные скобки у условного блока
                // trim применять нельзя, т.к. при наличии вложенных блоков сломается синтаксис
                $subQueryWithoutBrackets = substr($matches[0], 1, -1);
                $replacementResult = $this->buildSubQuery($subQueryWithoutBrackets, array_slice($args, $argIndex));

                // сколько аргументов потребовалось для блока; сдвигаем "аргументный" указатель
                $argIndex += $replacementResult->argsOffset;

                // если в условном блоке были аргументы, которые надо пропустить, то заменяем блок на пустую строку
                return $replacementResult->skippedValues
                    ? ''
                    : $replacementResult->query;
            }
        }, $query);

        return new ReplacementResult($query, $argIndex, $skippedValues);
    }

    /**
     * @throws SkipValueException
     * @throws \Exception
     */
    private function replaceId(string $match, mixed $argument): mixed
    {
        if ($argument instanceof SkipValue) {
            throw new SkipValueException();
        }

        $id = substr($match, 1, 1);
        if (!in_array($id, ['d', 'f', 'a', '#', ' ', ''], true)) {
            throw new \Exception(sprintf('Wrong identification [%s]', $id));
        }

        $argumentAsArray = (array)$argument;

        return match ($id) {
            'd' => is_null($argument) ? self::NULL_VALUE : (int)$argument,
            'f' => is_null($argument) ? self::NULL_VALUE : (float)$argument,
            'a' => implode(', ', array_map(
                fn (string|int $key, mixed $value): string => vsprintf('%s%s', [
                    is_numeric($key) ? '' : sprintf('`%s` = ', $this->escape($key)),
                    $this->restrictValue($value),
                ]),
                array_keys($argumentAsArray),
                array_values($argumentAsArray)
            )),
            '#' => implode(', ', array_map(
                fn (string $id): string => sprintf('`%s`', $this->escape($id)),
                $argumentAsArray
            )),
            default => $this->restrictValue($argument) . $id
        };
    }

    /**
     * @throws \Exception
     */
    private function restrictValue(mixed $value): mixed
    {
        if (!in_array($type = gettype($value), ['integer', 'double', 'boolean', 'string', 'NULL'], true)) {
            throw new \Exception(sprintf('Invalid argument type [%s]', $type));
        }

        return match ($type) {
            'boolean' => (int)$value,
            'NULL' => self::NULL_VALUE,
            'string' => sprintf("'%s'", $this->escape($value)),
            default => $value
        };
    }

    private function escape(string $str): string
    {
        return $this->mysqli->real_escape_string($str);
    }
}
