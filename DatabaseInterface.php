<?php

namespace FpDbTest;

interface DatabaseInterface
{
    /**
     * @throws \Exception
     */
    public function buildQuery(string $query, array $args = []): string;

    public function skip(): SkipValue;
}
