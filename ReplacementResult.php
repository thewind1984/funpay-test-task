<?php

namespace FpDbTest;

final readonly class ReplacementResult
{
    public function __construct(
        public string $query,
        public int $argsOffset,
        public bool $skippedValues,
    ) {}
}
